<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230712092350 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Ajout des relations au sein de la base de données';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE appart ADD groupe_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE appart ADD CONSTRAINT FK_E3F332497A45358C FOREIGN KEY (groupe_id) REFERENCES groupe (id)');
        $this->addSql('CREATE INDEX IDX_E3F332497A45358C ON appart (groupe_id)');
        $this->addSql('ALTER TABLE membre_gang ADD gang_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE membre_gang ADD CONSTRAINT FK_A62ABF5C9266B5E FOREIGN KEY (gang_id) REFERENCES gang (id)');
        $this->addSql('CREATE INDEX IDX_A62ABF5C9266B5E ON membre_gang (gang_id)');
        $this->addSql('ALTER TABLE objet ADD appart_id INT DEFAULT NULL, ADD categorie_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE objet ADD CONSTRAINT FK_46CD4C38D0CE844B FOREIGN KEY (appart_id) REFERENCES appart (id)');
        $this->addSql('ALTER TABLE objet ADD CONSTRAINT FK_46CD4C38BCF5E72D FOREIGN KEY (categorie_id) REFERENCES categorie (id)');
        $this->addSql('CREATE INDEX IDX_46CD4C38D0CE844B ON objet (appart_id)');
        $this->addSql('CREATE INDEX IDX_46CD4C38BCF5E72D ON objet (categorie_id)');
        $this->addSql('ALTER TABLE transaction ADD groupe_id INT DEFAULT NULL, ADD pm_id INT DEFAULT NULL, ADD gang_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE transaction ADD CONSTRAINT FK_723705D17A45358C FOREIGN KEY (groupe_id) REFERENCES groupe (id)');
        $this->addSql('ALTER TABLE transaction ADD CONSTRAINT FK_723705D16FBC242E FOREIGN KEY (pm_id) REFERENCES petite_main (id)');
        $this->addSql('ALTER TABLE transaction ADD CONSTRAINT FK_723705D19266B5E FOREIGN KEY (gang_id) REFERENCES gang (id)');
        $this->addSql('CREATE INDEX IDX_723705D17A45358C ON transaction (groupe_id)');
        $this->addSql('CREATE INDEX IDX_723705D16FBC242E ON transaction (pm_id)');
        $this->addSql('CREATE INDEX IDX_723705D19266B5E ON transaction (gang_id)');
        $this->addSql('ALTER TABLE user ADD groupe_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D6497A45358C FOREIGN KEY (groupe_id) REFERENCES groupe (id)');
        $this->addSql('CREATE INDEX IDX_8D93D6497A45358C ON user (groupe_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE appart DROP FOREIGN KEY FK_E3F332497A45358C');
        $this->addSql('DROP INDEX IDX_E3F332497A45358C ON appart');
        $this->addSql('ALTER TABLE appart DROP groupe_id');
        $this->addSql('ALTER TABLE membre_gang DROP FOREIGN KEY FK_A62ABF5C9266B5E');
        $this->addSql('DROP INDEX IDX_A62ABF5C9266B5E ON membre_gang');
        $this->addSql('ALTER TABLE membre_gang DROP gang_id');
        $this->addSql('ALTER TABLE objet DROP FOREIGN KEY FK_46CD4C38D0CE844B');
        $this->addSql('ALTER TABLE objet DROP FOREIGN KEY FK_46CD4C38BCF5E72D');
        $this->addSql('DROP INDEX IDX_46CD4C38D0CE844B ON objet');
        $this->addSql('DROP INDEX IDX_46CD4C38BCF5E72D ON objet');
        $this->addSql('ALTER TABLE objet DROP appart_id, DROP categorie_id');
        $this->addSql('ALTER TABLE transaction DROP FOREIGN KEY FK_723705D17A45358C');
        $this->addSql('ALTER TABLE transaction DROP FOREIGN KEY FK_723705D16FBC242E');
        $this->addSql('ALTER TABLE transaction DROP FOREIGN KEY FK_723705D19266B5E');
        $this->addSql('DROP INDEX IDX_723705D17A45358C ON transaction');
        $this->addSql('DROP INDEX IDX_723705D16FBC242E ON transaction');
        $this->addSql('DROP INDEX IDX_723705D19266B5E ON transaction');
        $this->addSql('ALTER TABLE transaction DROP groupe_id, DROP pm_id, DROP gang_id');
        $this->addSql('ALTER TABLE `user` DROP FOREIGN KEY FK_8D93D6497A45358C');
        $this->addSql('DROP INDEX IDX_8D93D6497A45358C ON `user`');
        $this->addSql('ALTER TABLE `user` DROP groupe_id');
    }
}
