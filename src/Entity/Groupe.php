<?php

namespace App\Entity;

use App\Repository\GroupeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: GroupeRepository::class)]
class Groupe
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\Column]
    private ?int $caisse_noir = null;

    #[ORM\OneToMany(mappedBy: 'groupe', targetEntity: User::class)]
    private Collection $users;

    #[ORM\OneToMany(mappedBy: 'groupe', targetEntity: Appart::class)]
    private Collection $appart;

    #[ORM\OneToMany(mappedBy: 'groupe', targetEntity: Transaction::class)]
    private Collection $transaction;

    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->appart = new ArrayCollection();
        $this->transaction = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getCaisseNoir(): ?int
    {
        return $this->caisse_noir;
    }

    public function setCaisseNoir(int $caisse_noir): static
    {
        $this->caisse_noir = $caisse_noir;

        return $this;
    }

    /**
     * @return Collection<int, User>
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): static
    {
        if (!$this->users->contains($user)) {
            $this->users->add($user);
            $user->setGroupe($this);
        }

        return $this;
    }

    public function removeUser(User $user): static
    {
        if ($this->users->removeElement($user)) {
            // set the owning side to null (unless already changed)
            if ($user->getGroupe() === $this) {
                $user->setGroupe(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Appart>
     */
    public function getAppart(): Collection
    {
        return $this->appart;
    }

    public function addAppart(Appart $appart): static
    {
        if (!$this->appart->contains($appart)) {
            $this->appart->add($appart);
            $appart->setGroupe($this);
        }

        return $this;
    }

    public function removeAppart(Appart $appart): static
    {
        if ($this->appart->removeElement($appart)) {
            // set the owning side to null (unless already changed)
            if ($appart->getGroupe() === $this) {
                $appart->setGroupe(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Transaction>
     */
    public function getTransaction(): Collection
    {
        return $this->transaction;
    }

    public function addTransaction(Transaction $transaction): static
    {
        if (!$this->transaction->contains($transaction)) {
            $this->transaction->add($transaction);
            $transaction->setGroupe($this);
        }

        return $this;
    }

    public function removeTransaction(Transaction $transaction): static
    {
        if ($this->transaction->removeElement($transaction)) {
            // set the owning side to null (unless already changed)
            if ($transaction->getGroupe() === $this) {
                $transaction->setGroupe(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        return $this->name;
    }

}
