<?php

namespace App\Entity;

use App\Repository\GangRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: GangRepository::class)]
class Gang
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\OneToMany(mappedBy: 'gang', targetEntity: Transaction::class)]
    private Collection $transactions;

    #[ORM\OneToMany(mappedBy: 'gang', targetEntity: MembreGang::class)]
    private Collection $membre_gang;

    public function __construct()
    {
        $this->transactions = new ArrayCollection();
        $this->membre_gang = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, Transaction>
     */
    public function getTransactions(): Collection
    {
        return $this->transactions;
    }

    public function addTransaction(Transaction $transaction): static
    {
        if (!$this->transactions->contains($transaction)) {
            $this->transactions->add($transaction);
            $transaction->setGang($this);
        }

        return $this;
    }

    public function removeTransaction(Transaction $transaction): static
    {
        if ($this->transactions->removeElement($transaction)) {
            // set the owning side to null (unless already changed)
            if ($transaction->getGang() === $this) {
                $transaction->setGang(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, MembreGang>
     */
    public function getMembreGang(): Collection
    {
        return $this->membre_gang;
    }

    public function addMembreGang(MembreGang $membreGang): static
    {
        if (!$this->membre_gang->contains($membreGang)) {
            $this->membre_gang->add($membreGang);
            $membreGang->setGang($this);
        }

        return $this;
    }

    public function removeMembreGang(MembreGang $membreGang): static
    {
        if ($this->membre_gang->removeElement($membreGang)) {
            // set the owning side to null (unless already changed)
            if ($membreGang->getGang() === $this) {
                $membreGang->setGang(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        return $this->name;
    }
}
