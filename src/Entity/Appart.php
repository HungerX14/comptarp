<?php

namespace App\Entity;

use App\Repository\AppartRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: AppartRepository::class)]
class Appart
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\ManyToOne(inversedBy: 'appart')]
    private ?Groupe $groupe = null;

    #[ORM\OneToMany(mappedBy: 'appart', targetEntity: Objet::class)]
    private Collection $objet;

    public function __construct()
    {
        $this->objet = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getGroupe(): ?Groupe
    {
        return $this->groupe;
    }

    public function setGroupe(?Groupe $groupe): static
    {
        $this->groupe = $groupe;

        return $this;
    }

    /**
     * @return Collection<int, Objet>
     */
    public function getObjet(): Collection
    {
        return $this->objet;
    }

    public function addObjet(Objet $objet): static
    {
        if (!$this->objet->contains($objet)) {
            $this->objet->add($objet);
            $objet->setAppart($this);
        }

        return $this;
    }

    public function removeObjet(Objet $objet): static
    {
        if ($this->objet->removeElement($objet)) {
            // set the owning side to null (unless already changed)
            if ($objet->getAppart() === $this) {
                $objet->setAppart(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        return $this->name;
    }
}
