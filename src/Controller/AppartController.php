<?php

namespace App\Controller;

use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AppartController extends AbstractController
{
    #[Route('/appart', name: 'app_appart')]
    public function index(Security $security): Response
    {
        $user = $security->getUser();

        return $this->render('appart/appart.html.twig', [
            'controller_name' => 'AppartController',
            'user' => $user,
        ]);
    
    }
}
