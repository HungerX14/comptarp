<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\Groupe;
use App\Entity\Transaction;
use App\Repository\UserRepository;
use App\Repository\GroupeRepository;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\TransactionRepository;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class HomeController extends AbstractController
{    
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    #[Route('/home', name: 'app_home')]
    public function index(Security $security, TransactionRepository $transactionRepository): Response
    {
        $user = $security->getUser();
        $transaction = $transactionRepository->findBy([],[],3);

        // Récupérer l'utilisateur actuellement authentifié via le bundle Security
        $user = $this->getUser();

        // Vérifier si l'utilisateur est connecté
        if (!$user) {
            return $this->redirectToRoute('app_login'); // Rediriger vers la page de connexion
        }

        // Accéder à l'id du groupe depuis l'utilisateur
        $groupId = $user->getGroupe()->getId();
        
        // Maintenant, vous pouvez accéder aux informations du groupe
        $groupe = $this->entityManager->getRepository(Groupe::class)->find($groupId);
        $transaction = $groupe->getTransaction();
        $apparts = $groupe->getAppart();

        $allObjet = [];
        
        foreach ($apparts as $appart) {
            $objetInAppart = $appart->getObjet();
            $allObjets = array_merge($allObjet, $objetInAppart->toArray());
        }

        //dd($objetInAppart);
        
        return $this->render('home/home.html.twig', [
            'controller_name' => 'HomeController',
            'user' => $user,
            'transactions' => $transaction,
            'groupe' => $groupe,
            'apparts' => $apparts,
            'objets' => $allObjets,
        ]);
    
    }
}
