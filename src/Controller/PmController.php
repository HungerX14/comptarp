<?php

namespace App\Controller;

use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class PmController extends AbstractController
{
    #[Route('/pm', name: 'app_pm')]
    public function index(Security $security): Response
    {
        $user = $security->getUser();

        return $this->render('pm/pm.html.twig', [
            'controller_name' => 'PmController',
            'user' => $user,
        ]);
    
    }
}
