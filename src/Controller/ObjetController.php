<?php

namespace App\Controller;

use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ObjetController extends AbstractController
{
    #[Route('/objet', name: 'app_objet')]
    public function index(Security $security): Response
    {
        $user = $security->getUser();

        return $this->render('objet/objet.html.twig', [
            'controller_name' => 'ObjetController',
            'user' => $user,
        ]);
    
    }
}
