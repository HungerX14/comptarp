<?php

namespace App\Controller;

use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class MembreGangController extends AbstractController
{
    #[Route('/membre/gang', name: 'app_membre_gang')]
    public function index(Security $security): Response
    {
        $user = $security->getUser();

        return $this->render('membre_gang/membre.html.twig', [
            'controller_name' => 'MembreGangController',
            'user' => $user,
        ]);
    
    }
}
