<?php

namespace App\Controller;

use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class GroupeController extends AbstractController
{
    #[Route('/groupe', name: 'app_groupe')]
    public function index(Security $security): Response
    {
        $user = $security->getUser();

        return $this->render('groupe/groupe.html.twig', [
            'controller_name' => 'GroupeController',
            'user' => $user,
        ]);
    
    }
}
