<?php

namespace App\Controller;

use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class TransactionController extends AbstractController
{
    #[Route('/transaction', name: 'app_transaction')]
    public function index(Security $security): Response
    {
        $user = $security->getUser();

        return $this->render('transaction/transaction.html.twig', [
            'controller_name' => 'TransactionController',
            'user' => $user,
        ]);
    
    }
}
