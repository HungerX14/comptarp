<?php

namespace App\Controller\Admin;

use App\Entity\Gang;
use App\Entity\User;
use App\Entity\Objet;
use App\Entity\Appart;
use App\Entity\Groupe;
use App\Entity\Categorie;
use App\Entity\MembreGang;
use App\Entity\PetiteMain;
use App\Entity\Transaction;
use App\Controller\Admin\UserCrudController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;

class DashboardController extends AbstractDashboardController
{
    #[Route('/admin', name: 'admin')]
    public function index(): Response
    {
        $adminUrlGenerator = $this->container->get(AdminUrlGenerator::class);
        return $this->redirect($adminUrlGenerator->setController(UserCrudController::class)->generateUrl());
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Comptarp');
    }

    public function configureMenuItems(): iterable
    {
        //yield MenuItem::linkToDashboard('Dashboard', 'fa fa-home');
        yield MenuItem::linkToCrud('Utilisateurs', 'fas fa-user', User::class);
        yield MenuItem::linkToCrud('Appartements', 'fas fa-house', Appart::class);
        yield MenuItem::linkToCrud('Categories', 'fas fa-list', Categorie::class);
        yield MenuItem::linkToCrud('Gangs', 'fas fa-building', Gang::class);
        yield MenuItem::linkToCrud('Groupes', 'fas fa-building', Groupe::class);
        yield MenuItem::linkToCrud('Membres des gangs', 'fas fa-people-group', MembreGang::class);
        yield MenuItem::linkToCrud('Petite mains', 'fas fa-user', PetiteMain::class);
        yield MenuItem::linkToCrud('Objets', 'fas fa-box', Objet::class);
        yield MenuItem::linkToCrud('Transactions', 'fas fa-dollar-sign', Transaction::class);
    }
}
