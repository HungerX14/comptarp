<?php

namespace App\Controller\Admin;

use App\Entity\Appart;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;

class AppartCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Appart::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('name'),
            AssociationField::new('groupe'),
        ];
    }

}
