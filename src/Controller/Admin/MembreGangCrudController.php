<?php

namespace App\Controller\Admin;

use App\Entity\MembreGang;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;

class MembreGangCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return MembreGang::class;
    }

    
    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('name'),
            TextField::new('telephone'),
            AssociationField::new('gang'),

        ];
    }
}
