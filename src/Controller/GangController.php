<?php

namespace App\Controller;

use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class GangController extends AbstractController
{
    #[Route('/gang', name: 'app_gang')]
    public function index(Security $security): Response
    {
        $user = $security->getUser();

        return $this->render('gang/gang.html.twig', [
            'controller_name' => 'GangController',
            'user' => $user,
        ]);
    
    }
}
