<?php

namespace App\DataFixtures;

use App\Entity\Appart;
use App\Entity\Groupe;
use App\Entity\Objet;
use App\Entity\User;
use App\Entity\Categorie;
use App\Entity\Gang;
use App\Entity\MembreGang;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory as Faker;
use PHPUnit\TextUI\XmlConfiguration\Group;
use Symfony\Component\PasswordHasher\Hasher\PasswordHasherFactoryInterface;

class AppFixtures extends Fixture
{
    public function __construct(
            private PasswordHasherFactoryInterface $passwordHasherFactory,
            ) {
            }

    public function load(ObjectManager $manager): void
    {
        $faker = Faker::create('fr_FR');
        $faker->addProvider(new \FakerEcommerce\Ecommerce($faker));

        //Création des données pour un admin

        //groupe
        $groupeAdmin = new Groupe();
        $groupeAdmin->setName('AdminGroupe');
        $groupeAdmin->setCaisseNoir(1000);

        $manager->persist($groupeAdmin);
        
        //admin
        $admin = new User();
        $admin->setRoles(['admin']);
        $admin->setName('Admin');
        $admin->setRoles(['ROLE_ADMIN']);
        $admin->setEmail('admin@test.com');
        $admin->setArgent(1000);
        $admin->setGroupe($groupeAdmin);
        $admin->setPassword($this->passwordHasherFactory->getPasswordHasher(User::class)->hash('admin'));

        $manager->persist($admin);

        function tabRandom($tabs){
            $tabRandom = array_rand($tabs);
            return $tabs[$tabRandom];
        }

        //Création des groupes

        $groupes = [];
        for ($i=0; $i < 5 ; $i++) { 
            $groupe = new Groupe();
            $groupe->setName($faker->company);
            $groupe->setCaisseNoir(random_int(1000, 1000000));
            $manager->persist($groupe);

            $groupes[] = $groupe;
        }
        
        //Création joueur

        for ($u=0; $u <20 ; $u++) {
            $user = new User();
            $user->setName($faker->name);
            $user->setEmail($faker->email);
            $user->setArgent(random_int(0,100000));
            $user->setGroupe(tabRandom($groupes));
            $user->setRoles(['ROLE_USER']);
            $user->setPassword($this->passwordHasherFactory->getPasswordHasher(User::class)->hash('userpassword'));
            $manager->persist($user);
        }

        //Création des appartements

        $apparts = [];

        for ($i=0; $i <4 ; $i++) { 
            $appart = new Appart();
            $appart->setGroupe(tabRandom($groupes));
            $appart->setName($faker->address);
            $manager->persist($appart);

            $apparts[] = $appart;
        }

        //Création des catégories

        $categories = [];

        for ($i=0; $i < 3 ; $i++) { 
            $categorie = new Categorie();
            $categorie->setName($faker->title);
            $manager->persist(($categorie));

            $categories[] = $categorie;
        }

        //Création des objets

        for ($i=0; $i < 200; $i++) { 
            $objet = new Objet();
            $objet->setName($faker->jewelry());
            $objet->setCategorie(tabRandom($categories));
            $objet->setAppart(tabRandom($apparts));
            $objet->setQuantite(random_int(0, 1000));

            $manager->persist($objet);
            
        }

        //Création gang

        $gangs = [];

        for ($i=0; $i < 5; $i++) { 
            $gang = new Gang();
            $gang->setName($faker->company);
            $manager->persist($gang);

            $gangs[] = $gang;
        }

        //Création des membres gang 

        for ($i=0; $i < 50; $i++) { 
            $membre_gang = new MembreGang();
            $membre_gang->setGang(tabRandom($gangs));
            $membre_gang->setName($faker->lastName);
            $membre_gang->setTelephone('555-'.random_int(1000,9999));
            $manager->persist($membre_gang);
        }

        $manager->flush();
    }
}
